game:GetService("StarterGui"):SetCore("SendNotification",{
 Title = "Executed!",
 Text = "",
 Icon = "",
 Duration = 5
})

local player = game:GetService("Players").LocalPlayer
local powerups = game:GetService("Workspace").PowerUps
local z = game:GetService("Workspace").Zombies
local r = game:GetService("ReplicatedStorage").RemoteEvent

local function kiwBus()
 local busFold = false
 local bus = false
 local busHum = false
 local busHead = false
 
 wait(15)
 
 repeat
  busFold = game.Workspace.Map:FindFirstChild("BossFolder")
  wait(1)
 until busFold; print("BossFolder found! (1/4)")
 
 repeat
  bus = busFold:FindFirstChild("Boss")
  wait(1)
 until bus; print("Boss found! (2/4)")

 repeat
  busHum = bus:FindFirstChild("Humanoid")
  wait(1)
 until busHum; print("Boss humanoid found! (3/4)")

 repeat
  busHead = bus:FindFirstChild("Head")
  wait(1)
 until busHead; print("Boss head found! (4/4)")

 local args = {
  [1] = "Fired",
  [2] = {
   [1] = busHead
  },
  [3] = busHead.Position
 }
 
 while busHum.Health > 0 do
  r:FireServer(unpack(args))
  r:FireServer(unpack(args))
  r:FireServer(unpack(args))
  r:FireServer(unpack(args))
  r:FireServer(unpack(args))
  wait(.1)
 end
 
 game:GetService("StarterGui"):SetCore("SendNotification",{
   Title = "Boss died!",
   Text = ":D",
   Icon = "",
   Duration = 3
  })
end

local round = player.PlayerGui.ScreenGui.MatchmakingFrame.Round

round:GetPropertyChangedSignal("Text"):Connect(function()
 print("Round "..round.Text)
 if (round.Text % 7) == 0 then
  spawn(kiwBus)
  warn("=> Boss round!")
  game:GetService("StarterGui"):SetCore("SendNotification",{
   Title = "Boss round!",
   Text = "Boss has arrived!",
   Icon = "",
   Duration = 3
  })
 end
end)

powerups.ChildAdded:Connect(function(obj)
 if obj:IsA("BasePart") then
  wait(1)
  obj.CFrame = CFrame.new(player.Character.PrimaryPart.Position)
 end
end)

spawn(function()
 while true do
  for _,v in pairs(z:GetChildren())do
   local hum = v:FindFirstChildOfClass("Humanoid")
   if hum and hum.Health > 0 and v:FindFirstChild("Head") then
    local args = {
     [1] = "Fired",
     [2] = {
      [1] = v.Head
     },
     [3] = v.Head.Position
    }
    r:FireServer(unpack(args))
   end
  end
  wait(0.1)
 end
end)