local foot = nil
local ts = nil
 
repeat
    wait()
    foot = script.Parent:FindFirstChild("Right Leg") or script.Parent:FindFirstChild("RightFoot")
    ts = script.Parent:FindFirstChild("Torso") or script.Parent:FindFirstChild("UpperTorso") 
until foot ~= nil and ts ~= nil
 
function toPos(num)
    local v = tostring(num)
    if string.sub(v,1,1) == "-" then
        return tonumber(string.sub(v,2,#v))
    else
        return num
    end
end
 
local hum = ts.Parent:FindFirstChild("Humanoid")
local maxFall = 0
local lastFall = false
 
hum.FreeFalling:connect(function(p)
    lastFall = p
    if p == true then
        while lastFall == true do
            wait(0.1)
            maxFall = toPos(ts.Velocity.Y)
        end
    else
        if maxFall >= math.random(61,78) then
            hum.Health = hum.Health - toPos(ts.Velocity.Y)*math.random(86,98)/math.random(98,126)
        end
        maxFall = 0
    end
end)